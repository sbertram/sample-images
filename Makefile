NULL=
EMPTY :=
SPACE := $(EMPTY) $(EMPTY)
COMMA := ,
ESCAPEDSPACE := \$(SPACE)
ESCAPEDSPACE_REPLACEMENT := @@SPACE@@

# This is useful to handle space in forech rules
handle-escaped-space = $(subst $(ESCAPEDSPACE),$(ESCAPEDSPACE_REPLACEMENT),$1)
apply-escaped-space = $(subst $(ESCAPEDSPACE_REPLACEMENT),$(SPACE),$1)

# Compute some system info
HOST_ARCH:=$(shell arch)
UIDGID := "$(shell id -u):$(shell id -g)"

# if automotive-image-builder subdir exists, use that a-i-b, otherwise fall back to PATH
ifneq (,$(wildcard automotive-image-builder/automotive-image-builder))
AIB=automotive-image-builder/automotive-image-builder
else
AIB=automotive-image-builder
endif

# Compute lists of available things
ARCHES := x86_64 aarch64
TARGETS := $(shell ${AIB} list-targets --quiet)
DISTROS := $(shell ${AIB} list-dist --quiet)
IMAGETYPES := regular ostree
FORMATS := img qcow2 oci.tar repo oci-archive rootfs ext4 tar aboot oci-archive+qcow2 oci-archive+img rpmlist
MANIFESTS := $(wildcard images/*.mpp.yml)

IMAGEDIR=

# Basic output directories
BUILDDIR=_build
OUTPUTDIR=$(BUILDDIR)/image_output
OSBUILD_BUILDDIR?=$(BUILDDIR)
STOREDIR=$(OSBUILD_BUILDDIR)/osbuild_store

# Allow specifying extra defines as makefile arguments
DEFINES=
DEFINE_FILE=
EXTEND_DEFINES=
MPP_ARGS=
DEFINES_ARGS:=$(foreach def,$(call handle-escaped-space,$(DEFINES)),--define '$(call apply-escaped-space,$(def))') $(foreach df,$(DEFINE_FILE),--define-file '$(df)') $(foreach def,$(call handle-escaped-space,$(EXTEND_DEFINES)),--extend-define '$(call apply-escaped-space,$(def))')
MPP_ARGS=
MPP_ARGS_ESCAPED:=$(shell for def in $(MPP_ARGS) ; do echo -n "--mpp-arg=$${def@Q} "; done)

# Allow specifying custom max cache size
CACHESIZE = 1GB

AIB_GEN_ARGS = $(DEFINES_ARGS) ${MPP_ARGS_ESCAPED}
AIB_BUILD_ARGS = --build-dir=$(OSBUILD_BUILDDIR) --cache-max-size=$(CACHESIZE) $(AIB_GEN_ARGS)

# Allow specifying extra ostree repo for output when building an image
ifdef OSTREE_REPO
AIB_BUILD_ARGS += --ostree-repo $(OSTREE_REPO)
endif

# We pre-create all the toplevel dirs, so that they are owned by the user, not root (when created under sudo)
.PHONY: $(BUILDDIR)
$(BUILDDIR):
	@mkdir -p $(BUILDDIR)
	@mkdir -p $(STOREDIR)/{objects,refs,sources/org.osbuild.files,tmp}
	@mkdir -p $(OUTPUTDIR)

# For things that depend on external things (like dnf repos, etc) that we can't properly check
-PHONY: rebuild-always
rebuild-always:

$(BUILDDIR)/%.json: $(BUILDDIR) rebuild-always
	$(AIB) --verbose compose ${shell tools/flags-from-filename $@} ${AIB_GEN_ARGS} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# A full set of partitions in qcow2 format
%.qcow2: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# A full set of partitions in raw image format
%.img: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# A container with the rootfs
%.oci.tar: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# A container oci archive with the rootfs
%.oci-archive: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# oci-archive and qcow2
%.oci-archive+qcow2: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# oci-archive and img
%.oci-archive+img: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# An ostree repository with the rootfs commit
%.repo: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# The rootfs as a directory
%.rootfs: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# A single-partition ext4 image
%.ext4: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# A tarfile of the rootfs
%.tar: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

# A directory with the android-style boot and system partition images
%.aboot: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

%.rpmlist: $(BUILDDIR) rebuild-always
	$(AIB) --verbose build ${shell tools/flags-from-filename $@} ${AIB_BUILD_ARGS} ${shell tools/buildflags-from-filename $@} ${shell tools/image-from-filename "${IMAGEDIR}" $@} $@

%.aboot.simg : %.aboot
	img2simg $</rootfs.img $</rootfs.simg
	rm $</rootfs.img

%.ext4.simg : %.ext4
	img2simg $< $@
	rm $<

%.simg : %.img
	img2simg $< $@
	rm $<

%.tar.gz : %.tar
	gzip -f $<

help:
	@echo
	@echo This makefile is a wrapper around automotive-image-builder and osbuild to easily build images
	@echo
	@echo To build a raw image, run \"make image.img\", or to build a qcow2 image, run \"make image.qcow2\".
	@echo This will build any image from a file called \"image.mpp.yml\" in the \"images\" subdirectory.
	@echo
	@echo For example, to build a minimal qcow2 image with ostree support targeting qemu, run:
	@echo '  make cs9-qemu-minimal-ostree.$(HOST_ARCH).qcow2'
	@echo
	@echo Other extensions are also supported:
	@echo \ \* .repo: Generate a repo with an ostree commit \(only works for ostree targets\)
	@echo \ \* .rootfs: Generate a directory with the rootfs content
	@echo \ \* .tar: Generate a tar file with the rootfs content
	@echo \ \* .ext4: Generate an ext4 filesystem with the rootfs content  \(size from \"image_size\"\)
	@echo \ \* .oci.tar: Generate an oci container image with the rootfs content
	@echo \ \* .oci-archive: Generate an oci container image with the ostree commit
	@echo \ \* .oci-archive+qcow2: Generate an oci container image with the ostree commit plus a qcow image deploying the commit
	@echo \ \* .oci-archive+img: Generate an oci container image with the ostree commit plus a image deploying the commit
	@echo \ \* .aboot: Generate an ext4 root filesystem and an android boot image \(for aboot targets\)
	@echo \ \* .rpmlist: Generate an json file listing the rpms used in the image
	@echo
	@echo You can pass variable declarations to osbuild-mpp with the DEFINES or DEFINE_FILE make variables.
	@echo DEFINE_FILE takes as argument a filename to a json dict file that contains the variables to set.
	@echo Multiple defines in DEFINES are separated by space, if you need a space inside a value escape it using \"\\ \".
	@echo For example, to add extra rpms to a minimal regular image, use:
	@echo "  make cs9-qemu-minimal-regular.$(HOST_ARCH).qcow2 DEFINES='extra_rpms=[\"gdb\",\"strace\"]'"
	@echo
	@echo To easily run the image with qemu, you can use the image runner tool, like:
	@echo \ \ automotive-image-runner cs9-qemu-minimal-regular.$(HOST_ARCH).qcow2
	@echo
	@echo There are some additional targets:
	@echo  \ \ clean_caches: Removes intermediate image build artifacts \(that improve rebuild speed\)
	@echo  \ \ clean_downloads: Removes files downloaded during image builds
	@echo  \ \ clean: Run clean_caches and clean_downloads
	@echo
	@echo There are also some common conversion rules:
	@echo  \ \ foo.aboot.simg will build aboot.img / rootfs.img and convert rootfs.img using img2simg
	@echo  \ \ foo.ext4.simg will build foo.ext4 and then convert it with img2simg
	@echo  \ \ foo.simg will build foo.img and then convert it with img2simg
	@echo  \ \ foo.tar.gz will build $foo.tar and then gzip it
	@echo
	@echo "When building a custom variant of an image (say with an extra package) you can use a"
	@echo custom @suffix to change the name of the produced file. For example:
	@echo "  make cs9-qemu-minimal-ostree@gdb.$(HOST_ARCH).qcow2 DEFINES='extra_rpms=[\"gdb\"]'"
	@echo

.PHONY: clean_downloads
clean_downloads:
	sudo rm -rf $(STOREDIR)/sources/org.osbuild.files/*

.PHONY: clean_caches
clean_caches:
	sudo rm -rf $(STOREDIR)/refs/*
	sudo rm -rf $(STOREDIR)/refs_tars/*
	sudo rm -rf $(STOREDIR)/objects/*
	sudo rm -rf $(STOREDIR)/tmp/*
	sudo rm -rf $(STOREDIR)/image_output/*

.PHONY: clean
clean: clean_downloads clean_caches

# Template rule for producing core rpm lists
# $1 == distro name
# $2 == Arch
define core-rpms-rule
.PHONY: $1-core-rpms-$2.txt
$1-core-rpms-$2.txt:
	tools/generate-core-rpms $1 $2 > $$@
endef

$(foreach d, $(DISTROS), \
 $(foreach a,$(ARCHES), \
  $(eval $(call core-rpms-rule,$d,$a))))

AUTOSIGREPO=https://mirror.stream.centos.org/SIGs/9-stream/automotive
define u-boot-rule
qemu-u-boot-$1.bin:
	DLDIR=`mktemp -d` && \
	dnf download -q --downloaddir $$$$DLDIR --disablerepo "*" --repofrompath "autosig,${AUTOSIGREPO}/$1/packages-main/" autosig-u-boot && \
	rpm2cpio $$$$DLDIR/autosig-u-boot-*.rpm | cpio -id -D $$$$DLDIR && \
	mv $$$$DLDIR/boot/u-boot.bin $$@ && \
	rm -rf $$$$DLDIR
endef
$(foreach a,$(ARCHES), $(eval $(call u-boot-rule,$a)))
